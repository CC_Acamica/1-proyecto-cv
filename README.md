**Primer proyecto de la carrera Desarrollo Web Full Stack de Acámica. El objetivo es realizar un CV con HTML y CSS.**

---

## Objetivo del proyecto
A partir del uso de los lenguajes HTML (de marcado) y CSS (de estilado) vamos a crear nuestro primer sitio web. Este sitio va a ser tu presentación personal donde vas a poder mostrarle al mundo qué sabes hacer, qué hiciste, lo que aprendiste, etc. En este curso vas a encontrar una introducción al proyecto, la explicación de los recursos que debes descargar para completarlo y algunos ejemplos de CV para que puedas inspirarte y comenzar a crearlo.

---

## Proyecto finalizado
El proyecto finalizado se encuentra en el directorio ChristianCabrer-CVOnline.
Se puede ejecutar el mismo abriendo el archivo index.html en el navegador.

---

## Descripción de como se llevo a cabo el proyecto
* Primero comencé definiendo las secciones que iba a tener mi CV, dividiendolas con div y poniendole un class.
* Luego empece a volcar toda la información ordenandola en titulos, párrafos y listas dentro de las secciones correspondientes.
* Seguí con el CSS inicial, agregandole tamaños a las distintas secciones para que se vean como yo quería y algunos detalles iniciales.
* Luego definí el estilo que iba a tener mi CV. De "https://www.colourlovers.com/" tomé la imagen para el fondo y fui probando la paleta de colores hasta llegar a la que mas me gustaba para éste proyecto.
* Después en "https://getavataaars.com/" hice un avatar para poner como imagen de perfil y la agregue al CV. 
* De "https://www.freepik.es/" tomé los iconos para los datos personales de la sección de quien soy.
* Para terminar con el estilado utilicé "https://fonts.google.com/" para elegir las fuentes que iba a utilizar, después de varias pruebas encontré las que mas me gustaban para el proyecto, las descargué, las agregue al CSS y fui modificando la fuente y tamaño de cada parte del CV.
* Y finalmente tomé las imágenes para el portfolio de los 10 proyectos a realizar de la web de Acámica, las descargué y las agregué como 2 listas horizontales.

---